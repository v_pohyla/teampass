FROM centos:6.8
MAINTAINER pohyla
RUN yum install -y epel-release
RUN yum install -y yum-utils
RUN rpm -Uhv http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
RUN yum -y install https://centos6.iuscommunity.org/ius-release.rpm
RUN yum --disablerepo=* --enablerepo=remi,remi-php56,base,epel install -y \
           httpd \
           php \   
           php-cli \
           php-common \
           php-xml \
           php-xmlrpc \
           php-mysqlnd \
           php-mcrypt \
           php-mbstring \
           unzip \
           zip \
&& yum clean headers packages metadata all
RUN curl -O http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz ; \
 tar -xzf ioncube_loaders_lin_x86-64.tar.gz ; \
 cp /ioncube/ioncube_loader_lin_5.6.so /usr/lib64/php/modules ; \
 echo "zend_extension = /usr/lib64/php/modules/ioncube_loader_lin_5.6.so" >> /etc/php.ini ; \
 rm -rf /ioncube ; \
 rm -rf /ioncube_loaders_lin_x86-64.tar.gz 
RUN curl -O http://teampasswordmanager.com/assets/download/teampasswordmanager_6.68.138.zip ; \
 unzip teampasswordmanager_6.68.138.zip ; \
 mkdir -p /usr/local/www/ ; \
 mv /teampasswordmanager_6.68.138 /usr/local/www/teampasswordmanager 

RUN sed -i 's/localhost/mysql/g' /usr/local/www/teampasswordmanager/config.php ; \
 sed -i 's/user/team/g' /usr/local/www/teampasswordmanager/config.php ; \
 sed -i 's/password/teampass/g' /usr/local/www/teampasswordmanager/config.php ; \
 sed -i 's/database/team/g' /usr/local/www/teampasswordmanager/config.php

COPY teampasswordmanager.conf /etc/httpd/conf.d/
RUN rm -rf /etc/httpd/conf.d/welcome.conf
RUN touch /var/log/php_errors.log && chown apache:apache /var/log/php_errors.log
RUN chown -R apache:apache /usr/local/www/teampasswordmanager
RUN echo "always_populate_raw_post_data=-1" >> /etc/php.ini ; \
 echo "error_log = /var/log/php_errors.log" >> /etc/php.ini ; \
 echo "date.timezone = UTC" >> /etc/php.ini 

WORKDIR /usr/local/www/teampasswordmanager
EXPOSE 80 443
CMD /usr/sbin/httpd -D FOREGROUND

  
